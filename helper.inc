<?php

/**
 * @file
 * helper.inc
 */
function _html5lib_get_library_path() {

  $file = 'Parser.php';
  $library = 'html5lib';

  // Implement simple cache.
  $library_path = &drupal_static(__FUNCTION__);
  if (!empty($library_path)) {
    return $library_path;
  }

  // Support libraries module.
  if (module_exists('libraries') && function_exists('libraries_get_path')) {
    $library_path = libraries_get_path($library) . "/$file";
    if (file_exists($library_path)) {
      return $library_path;
    }
  }
  else {
    $paths = array(
      'sites/all/libraries/' . $library,
      drupal_get_path('module', 'html5lib') . '/' . $library,
      drupal_get_path('module', 'html5lib') . "/libraries",
      'profiles/' . variable_get('install_profile', 'default') . '/libraries/' . $library,
    );
    foreach ($paths as $library_dir) {
      $library_path = $library_dir . "/$file";
      if (file_exists($library_path)) {
        return $library_path;
      }
    }
  }
  return FALSE;
}
